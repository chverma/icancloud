#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import shutil, errno
import os
from os import listdir
from os.path import isfile, join
from subprocess import call

SERVERAPPLICATION_DIRECTORY = "src/Applications/Apps/"
MAKE_FILE_ORIG = "Makefile.original"
MAKE_FILE_OUTPUT = "Makefile"

def copyFromSrcToDst(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise
      
name_app = "ServerApplication"

include_path  = "-Isrc/Applications/Apps/"+name_app

object_inc = "\$O/src/Applications/Apps/"+name_app+"/"+name_app+".o"

#clean = "$(Q)-rm -f src/Applications/Apps/"+name_app+"/*_m.cc src/Applications/Apps/"+name_app+"/*_m.h"
clean ="\$\(Q\)-rm -f src/Applications/Apps/"+name_app+"/\*_m.cc src/Applications/Apps/"+name_app+"/\*_m.h"

depends = "src/Applications/Apps/"+name_app+"/\*.cc"

object_maker = "$O/src/Applications/Apps/"+name_app+"/"+name_app+".o: src/Applications/Apps/"+name_app+"/"+name_app+".cc \\\n  \
	src/Applications/Apps/ServerApplication/ServerApplication.h \\\n  \
	src/Applications/Base/Management/JobQueue.h \\\n  \
	src/Applications/Base/Management/JobResults.h \\\n  \
	src/Applications/Base/Management/JobResultsSet.h \\\n  \
	src/Applications/Base/UserJob.h \\\n  \
	src/Applications/Base/jobBase.h \\\n  \
	src/Applications/Libraries_API/API_OS/API_OS.h \\\n  \
	src/Architecture/Machine/Machine.h \\\n  \
	src/Architecture/Node/AbstractNode.h \\\n  \
	src/Architecture/NodeComponents/OperatingSystems/SyscallManager/AbstractSyscallManager.h \\\n  \
	src/Architecture/NodeComponents/OperatingSystems/SyscallManager/NodeSyscallManager/SyscallManager.h \\\n  \
	src/Architecture/NodeComponents/OperatingSystems/SystemApps/RemoteStorageApp/RemoteStorageApp.h \\\n  \
	src/Architecture/NodeComponents/OperatingSystems/SystemApps/StatesApplication/StatesApplication.h \\\n  \
	src/Base/Messages/icancloud_App_CPU_Message.h \\\n  \
	src/Base/Messages/icancloud_App_CPU_Message_m.h \\\n  \
	src/Base/Messages/icancloud_App_IO_Message.h \\\n  \
	src/Base/Messages/icancloud_App_IO_Message_m.h \\\n  \
	src/Base/Messages/icancloud_App_MEM_Message.h \\\n  \
	src/Base/Messages/icancloud_App_MEM_Message_m.h \\\n  \
	src/Base/Messages/icancloud_App_NET_Message.h \\\n  \
	src/Base/Messages/icancloud_App_NET_Message_m.h \\\n  \
	src/Base/Messages/icancloud_BlockList_Message.h \\\n  \
	src/Base/Messages/icancloud_BlockList_Message_m.h \\\n  \
	src/Base/Messages/icancloud_File.h \\\n  \
	src/Base/Messages/icancloud_MPI_Message.h \\\n  \
	src/Base/Messages/icancloud_MPI_Message_m.h \\\n  \
	src/Base/Messages/icancloud_Message.h \\\n  \
	src/Base/Messages/icancloud_Message_m.h \\\n  \
	src/Base/Messages/icancloud_Migration_Message.h \\\n  \
	src/Base/Messages/icancloud_Migration_Message_m.h \\\n  \
	src/Base/Parser/cfgMPI.h \\\n  \
	src/Base/Request/Request.h \\\n  \
	src/Base/Util/Log/ICCLog.h \\\n  \
	src/Base/cGateManager.h \\\n  \
	src/Base/icancloud_Base.h \\\n  \
	src/Base/include/Constants.h \\\n  \
	src/Base/include/icancloud_debug.h \\\n  \
	src/Base/include/icancloud_types.h \\\n  \
	src/EnergySystem/EnergyMeter/EnergyMeterUnit/Memorization/Memoization_uthash.h \\\n  \
	src/EnergySystem/EnergyMeter/EnergyMeterUnit/Memorization/uthash.h \\\n  \
	src/Management/DataCenterManagement/Base/RequestsManagement.h \\\n  \
	src/Management/MachinesStructure/ElementType.h \\\n  \
	src/Users/AbstractUser.h \\\n  \
	src/Users/Base/queuesManager.h \\\n  \
	src/Users/Base/userBase.h \\\n  \
	src/Users/Base/userStorage.h \\\n  \
	$(INET_PROJ)/src/base/Compat.h \\\n  \
	$(INET_PROJ)/src/base/INETDefs.h \\\n  \
	$(INET_PROJ)/src/networklayer/contract/IPv4Address.h \\\n  \
	$(INET_PROJ)/src/networklayer/contract/IPv6Address.h \\\n  \
	$(INET_PROJ)/src/networklayer/contract/IPvXAddress.h \\\n  \
	$(INET_PROJ)/src/transport/contract/TCPCommand_m.h \\\n  \
	$(INET_PROJ)/src/transport/contract/TCPSocket.h"

object_maker_end = "\$\(INET_PROJ\)/src/transport/contract/TCPSocket.h"
object_maker_end_next = "\$O/src/Applications/Base/UserJob.o: src/Applications/Base/UserJob.cc"

fMake = open(MAKE_FILE_ORIG,'r')
make_file = fMake.read()

make_file_output = open(MAKE_FILE_OUTPUT,'w+')
#print re.search(name_app,make_file).end()

nAppServer = int(raw_input("Cuántas apps quieres?\n"))

print
print ("Creando %s apps ... " % nAppServer)
print

######## Search Include Path
print "Escribiendo include path"
startsApps =  [m.start() for m in re.finditer(include_path, make_file)]
endApps =  [m.end() for m in re.finditer(include_path, make_file)]

# Write first lines
make_file_output.write(make_file[:endApps[0]+2])
make_file_output.write("\n")

# Write Include Path
for i in range(1,nAppServer+1):
	make_file_output.write("    "+include_path+str(i)+" \\\n")
#print make_file[startsApps[0]:endApps[0]+2]



######## Search Object declaration
startsObj=  [m.start() for m in re.finditer(object_inc, make_file)]
endObj =  [m.end() for m in re.finditer(object_inc, make_file)]

# Write lines between include path & object declaration
make_file_output.write(make_file[endApps[0]+3:endObj[0]+3])
#make_file_output.write("\n")

# Write Object declaration
for i in range(1,nAppServer+1):
	make_file_output.write("    "+object_inc[1:].replace("ServerApplication","ServerApplication"+str(i))+" \\\n")
#print make_file[startsApps[0]:endApps[0]+2]



######## Search Clean
startClean =  [m.start() for m in re.finditer(clean, make_file)]
endClean =  [m.end() for m in re.finditer(clean, make_file)]

# Write lines between Object declaration & clean section
make_file_output.write(make_file[endObj[0]+3:endClean[0]+1])
#print make_file[startsApps[0]:endApps[0]]

# Write clean section
for i in range(1,nAppServer+1):
	make_file_output.write("\t$(Q)"+clean.replace("ServerApplication","ServerApplication"+str(i))[7:].replace("\\","")+"\n")
	
######## Search Dependencies
startsDepend =  [m.start() for m in re.finditer(depends, make_file)]
endDepend =  [m.end() for m in re.finditer(depends, make_file)]


# Write lines between Clean section & Dependencies
make_file_output.write(make_file[endClean[0]+1:endDepend[0]+1])

# Write Dependencies section
for i in range(1,nAppServer+1):
	make_file_output.write(depends.replace("ServerApplication","ServerApplication"+str(i)).replace("\\","")+" ")
	

######## Object Maker

# Search 
startsObject =  [m.start() for m in re.finditer(object_maker_end, make_file)]
endObject =  [m.start() for m in re.finditer(object_maker_end_next, make_file)]



mesAprop = -1
for elem in endObject:
	if startsObject[0]>=elem:
		mesAprop=elem

# Write lines between Dependencies & Object Maker
make_file_output.write(make_file[endDepend[0]+1:endObject[0]])
#print make_file[mesAprop:endApps[0]]

# Write Object maker section
for i in range(1,nAppServer+1):
	make_file_output.write(object_maker.replace("ServerApplication","ServerApplication"+str(i))+"\n")

# Write lines between Object Maker & EOF
make_file_output.write(make_file[endObject[0]:])

# Close files
make_file_output.close()
fMake.close()


############# Make File structure


for i in range(1,nAppServer+1):
	#copyFromSrcToDst(SERVERAPPLICATION_DIRECTORY+"ServerApplication",SERVERAPPLICATION_DIRECTORY+"ServerApplication"+str(i))
	try:
	  os.makedirs( SERVERAPPLICATION_DIRECTORY+"ServerApplication"+str(i) )
	  myPath = SERVERAPPLICATION_DIRECTORY+"ServerApplication"
	  #file_to_change = open(
	  onlyfiles = [ f for f in listdir(myPath) if isfile(join(myPath,f)) ]
	  for f in onlyfiles:
		  fileToRead = open(os.path.join(myPath,f),"r")
		  #shutil.copyfile(f, SERVERAPPLICATION_DIRECTORY+"ServerApplication"+str(i))
		  fileToWrite = open(os.path.join(myPath+str(i),f.replace("ServerApplication","ServerApplication"+str(i))),"w+")
		  text = fileToRead.read()
		  fileToWrite.write(text.replace("ServerApplication","ServerApplication"+str(i)))
		  fileToWrite.close()
	except OSError:
		pass

call(["make"])
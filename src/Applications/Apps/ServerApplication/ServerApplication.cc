
#include "ServerApplication.h"
#define INPUT_FILE "/web.html"
#define OUTPUT_FILE "/log.dat"
#define MAX_FILE_SIZE 200000000000
#define SM_WAIT_TO_EVENT  "Wait_To_Event"

Define_Module(ServerApplication);



ServerApplication::~ServerApplication(){
}


void ServerApplication::initialize(){

    // Init the super-class
    UserJob::initialize();

	std::ostringstream osStream;
    timeoutEvent = NULL;
    timeout = 1.0;

    // Set the moduleIdName
	osStream << "ServerApplication." << getId();
	moduleIdName = osStream.str();

	// App Module parameters
	startDelay = par ("startDelay");
	inputSize  = par ("inputSize");
	MIs  = par ("MIs");
    hitsPerHour = par ("hitsPerHour").longValue();
    uptimeLimit = par("uptimeLimit").longValue();
    intervalHit = (3600.0 / (double) hitsPerHour);

    pendingHits = 0;

    if (uptimeLimit != 0) uptimeLimit = uptimeLimit + simTime().dbl();

	// Service times
	total_service_IO = 0.0;
	total_service_CPU = 0.0;
	total_service_MEM = 0.0;
    
	startServiceIO = 0.0;
	endServiceIO = 0.0;
	startServiceCPU = 0.0;
	endServiceCPU = 0.0;
	startServiceMEM = 0.0;
	endServiceMEM = 0.0;
	readOffset = 0;
	totalOccupation=0;

	// Boolean variables
	executeCPU = executeRead =executeMEM= false;

    // Assign names to the results
    jobResults->newJobResultSet("totalIO");
    jobResults->newJobResultSet("totalCPU");
    jobResults->newJobResultSet("totalMEM");
    //jobResults->newJobResultSet("OccupationMEM");
    jobResults->newJobResultSet("Real run-time");
    jobResults->newJobResultSet("Simulation time");

}

void ServerApplication::startExecution (){

    API_OS::startExecution();
	// Create SM_WAIT_TO_EXECUTE message for delaying the execution of this application
    Enter_Method_Silent();
    // Initialize ..
    newIntervalEvent = new cMessage ("intervalEvent");
    
	cMessage *waitToExecuteMsg = new cMessage (SM_WAIT_TO_EXECUTE.c_str());
	scheduleAt (simTime()+startDelay, waitToExecuteMsg);
}

void ServerApplication::finish(){

	// Finish the super-class
	UserJob::finish();

}

void ServerApplication::processSelfMessage (cMessage *msg){

    SimTime nextEvent;

		if (!strcmp (msg->getName(), SM_WAIT_TO_EXECUTE.c_str())){

			// Starting time...
			simStartTime = simTime();
			runStartTime = time (NULL);
			cancelAndDelete(msg);
			// Init...
			scheduleAt (simTime()+ intervalHit, newIntervalEvent);

		} else if (!strcmp (msg->getName(), "intervalEvent")){

           newHit();

           // A set of hits each second
           if ((uptimeLimit >= simTime().dbl()) || (uptimeLimit == 0)){
               cancelEvent(msg);
               scheduleAt (simTime()+ intervalHit, newIntervalEvent);
           } else {
               uptimeLimit = -1;
               cancelAndDelete(msg);
           }

        }else{

			showErrorMessage ("Unknown self message [%s]", msg->getName());
		    cancelAndDelete(msg);
        }


}

void ServerApplication::processRequestMessage (icancloud_Message *sm){

}

void ServerApplication::processResponseMessage (icancloud_Message *sm){

	icancloud_App_IO_Message *sm_io;
	icancloud_App_CPU_Message *sm_cpu;
	icancloud_App_MEM_Message *sm_mem;
	bool isError;
	std::ostringstream osStream;
    int operation;


		// Init...
        operation = sm->getOperation ();
		sm_io = dynamic_cast<icancloud_App_IO_Message *>(sm);
		sm_cpu = dynamic_cast<icancloud_App_CPU_Message *>(sm);
		sm_mem = dynamic_cast<icancloud_App_MEM_Message *>(sm);
		isError = false;
		
		// MEM Message?
		if(sm_mem != NULL){
		  //printf("operation : %d; SM_MEM_ALLOCATE: %d \n",operation,SM_MEM_ALLOCATE);
		  executeCPU = false;
		  executeRead = true;
		  executeMEM = false;
		  endServiceMEM = simTime();
		  total_service_MEM += (endServiceMEM - startServiceMEM);
		  //totalOccupation = sm_mem->getMemoryOccupation();
		  //icancloud_request_freeMemory (inputSize*KB, readOffset);
		// IO Message?
		}else if (sm_io != NULL){

			// Get time!
			endServiceIO = simTime();

			// Read response!
			if (operation == SM_READ_FILE){
				// All ok!
				if (sm_io->getResult() == icancloud_OK){
					executeCPU = true;
					executeRead = false;
					executeMEM = false;
				}

				// File not found!
				else if (sm_io->getResult() == icancloud_FILE_NOT_FOUND){
					osStream << "File not found!";					isError = true;
				}

				// File not found!
				else if (sm_io->getResult() == icancloud_DATA_OUT_OF_BOUNDS){
					executeCPU = true;
					executeMEM = false;
				}

				// Unknown result!
				else{
					osStream << "Unknown result value:" << sm_io->getResult();
					isError = true;
				}
			}


			// Unknown I/O operation
			else{
				osStream << "Unknown received response message";
				isError = true;
			}

			// Increase total time for I/O
			total_service_IO += (endServiceIO - startServiceIO);

		}

		// Response came from CPU system
		else if (sm_cpu != NULL){

			// Get time!
			endServiceCPU = simTime ();

			// Increase total time for CPU
			total_service_CPU += (endServiceCPU - startServiceCPU);
			// CPU!
			if (operation == SM_CPU_EXEC){
				
				pendingHits--;
				if ((pendingHits == 0) && (uptimeLimit == -1)){
				  printResults();
				}
			}

			// Unknown CPU operation
			else{
				osStream << "Unknown received response message";
				isError = true;
			}

		}

		// Wrong response message!
		else{

			osStream << "Unknown received response message";
			isError = true;
		}
			//printf("executeRead: %s\n",executeRead ? "True" : "False");
			// Error?
			if (isError){

				showErrorMessage ("Error in response message:%s. %s",
									osStream.str().c_str(),
									sm_io->contentsToString(true).c_str());
			}
			//MEM
			else if(executeMEM){
				icancloud_request_allocMemory (inputSize*KB, readOffset);
			}
			// CPU?
			else if (executeCPU){

				// Execute CPU!
				executeCPUrequest ();
			}

			// IO?
			else if (executeRead){
					serveWebCode();
			}

			// Inconsistency error!
			else
				showErrorMessage ("Inconsistency error!!!! :%s. %s",
									osStream.str().c_str(),
									sm->contentsToString(true).c_str());

            delete (sm);
} 


void ServerApplication::changeState(string newState){

}


void ServerApplication::newHit(){
    pendingHits++;
    startServiceIO = simTime();
    //serveWebCode ();
    executeMEMrequest();
}


void ServerApplication::serveWebCode(){
Enter_Method_Silent();
	// Reset timer!
	startServiceIO = simTime();
	//printf("Serve WEB: readOffset: %d; MAX_FILE_SIZE:%lu\n",(readOffset+(inputSize*KB)),MAX_FILE_SIZE);
	// Executes read operation
		if ((readOffset+(inputSize*KB))>=MAX_FILE_SIZE)
			readOffset = 0;

		if (DEBUG_Application)
			showDebugMessage ("Executing (Read) Offset:%d; dataSize:%d", readOffset,  inputSize*KB);

		icancloud_request_read (INPUT_FILE, readOffset, inputSize*KB);
		
		readOffset += (inputSize*KB);

}


void ServerApplication::executeCPUrequest(){
    Enter_Method_Silent();

	// Debug?
	if (DEBUG_Application)
		showDebugMessage ("Executing (CPU) MIs:%d", MIs);

	// Reset timer!
	startServiceCPU = simTime ();
	icancloud_request_cpu (MIs);
}

void ServerApplication::executeMEMrequest(){
    Enter_Method_Silent();

	// Reset timer!
	startServiceMEM = simTime ();
	icancloud_request_allocMemory (inputSize*KB, SM_MEMORY_REGION_CODE);
	//icancloud_request_freeMemory (inputSize*KB, readOffset);
}

void ServerApplication::printResults (){

	std::ostringstream osStream;

	//Init..
		simEndTime = simTime();
		runEndTime = time (NULL);

	if (WRITE_MESSAGES_TO_FILE)
	  showResultMessage ("App [%s] - Simulation time:%f - Real execution time:%f - IO:%f  CPU:%f",
		                           moduleIdName.c_str(),
		                           (simEndTime-simStartTime).dbl(),
		                           (difftime (runEndTime,runStartTime)),
		                           total_service_IO.dbl(),
		                           total_service_CPU.dbl());

    //Assign values to the results
        osStream <<  total_service_IO.dbl();
        jobResults->setJobResult(0, osStream.str());
        osStream.str("");

        osStream <<  total_service_CPU.dbl();
        jobResults->setJobResult(1, osStream.str());
        osStream.str("");

	osStream <<  total_service_MEM.dbl();
        jobResults->setJobResult(2, osStream.str());
        osStream.str("");
	
	//osStream <<  (((totalBlocks - freeAppBlocks) * 100) / totalBlocks);
	/*osStream << totalOccupation;
        jobResults->setJobResult(3, osStream.str());
        osStream.str("");*/
	
        osStream <<  difftime (runEndTime,runStartTime);
        jobResults->setJobResult(3, osStream.str());
        osStream.str("");

        osStream << (simEndTime - simStartTime).dbl();
        jobResults->setJobResult(4, osStream.str());

        addResults(jobResults);
    //Send results list to the cloudManager
        userPtr->notify_UserJobHasFinished(this);

}

